# Programming Tools and Techniques.

This repository will contain all the material relevant for the
Programming tools and techniques course.

* Source available at: <https://bitbucket.org/ppk-teach/tools/>
* Wiki page:  <https://bitbucket.org/ppk-teach/tools/wiki/>
* Issue tracker: <https://bitbucket.org/ppk-teach/tools/issues/>
* IRC: `irc.freenode.net` channel `##cseiitk-programming-tools` (note
  there are two hashes in the begining)

## How to contribute?

See the wiki page <https://bitbucket.org/ppk-teach/tools/wiki/Contributing>
